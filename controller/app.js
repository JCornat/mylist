'use strict';

(function () {
    var app = angular.module('app', ['ui.router', 'ngAnimate', 'ngSanitize', 'ngTouch']);

    app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('movie-list', {
                url: '/movies',
                views: {
                    '': {
                        templateUrl: 'controller/movie/view/list.html',
                        controller: 'MovieListCtrl as ctrl'
                    },
                    'menu': {
                        templateUrl: 'controller/global/view/menu.html',
                        controller: 'MenuCtrl as ctrl'
                    }
                }
            })
            .state('game-list', {
                url: '/games/:id',
                views: {
                    '': {
                        templateUrl: 'controller/game/view/list.html',
                        controller: 'GameListCtrl as ctrl'
                    }
                }
            })
            .state('manga-list', {
                url: '/mangas',
                views: {
                    '': {
                        templateUrl: 'controller/patient/view/list.html',
                        controller: 'MangaListCtrl as ctrl'
                    },
                    'menu': {
                        templateUrl: 'controller/global/view/menu.html',
                        controller: 'MenuCtrl as ctrl'
                    }
                }
            });

        $urlRouterProvider.otherwise('movies');
    }]);

    app.run(['$rootScope', function($rootScope) {
        $rootScope.previousState = {};
        $rootScope.currentState = {};
        $rootScope.$on('$stateChangeSuccess', function(ev, to, toParams, from, fromParams) {
            $rootScope.previousState = {name: from.name, params: fromParams};
            $rootScope.currentState = {name: to.name, params: toParams};
        });
    }]);

    app.controller('MovieListCtrl', ['$rootScope', '$http', function ($rootScope, $http) {
        var self = this;

        $http({method: 'GET', url: 'asset/list.json'})
            .then(function(data, status) {
                self.movies = data.data.movies;
            }, function(data, status) {
                console.log(data || "Request failed");
            });


        self.defaultLimit = 30;
        self.limit = angular.copy(self.defaultLimit);
        self.filter = {name: 'title', value: ['title']};
        self.setFilter = function (name) {
            if (self.filter.name === name) {
                switch (name) {
                    case 'title':
                        name = '-title';
                        break;
                    case 'year':
                        name = '-year';
                        break;
                    case 'mark':
                        name = '-mark';
                        break;

                    case '-title':
                        name = 'title';
                        break;
                    case '-year':
                        name = 'year';
                        break;
                    case '-mark':
                        name = 'mark';
                        break;
                }
            }

            switch (name) {
                case 'title':
                    self.filter = {name: 'title', value: ['title']};
                    break;
                case 'year':
                    self.filter = {name: 'year', value: ['-release_date', 'title']};
                    break;
                case 'mark':
                    self.filter = {name: 'mark', value: ['-mark', 'title']};
                    break;

                case '-title':
                    self.filter = {name: '-title', value: ['-title']};
                    break;
                case '-year':
                    self.filter = {name: '-year', value: ['release_date', 'title']};
                    break;
                case '-mark':
                    self.filter = {name: '-mark', value: ['mark', 'title']};
                    break;
            }

            self.limit = angular.copy(self.defaultLimit);
        };

        self.toggleModal = function (boolean) {
            self.displayModal = (boolean === undefined) ? !self.displayModal : boolean;

            if (self.displayModal) {
                $('body').addClass('modal-open');
            } else {
                $('body').removeClass('modal-open');
            }
        };

        self.toggleSidebar = function () {
            $rootScope.$broadcast('toggleSidebar', true);
        };

        $rootScope.$on('improveLimit', function(event, data) {
            self.limit += self.defaultLimit;
        });

    }]);

    app.controller('GameListCtrl', ['$state', '$http', function ($state, $http) {
        var self = this;
    }]);

    app.controller('MenuCtrl', ['$rootScope', function ($rootScope) {
        var self = this;

        self.toggleSidebar = function (boolean) {
            self.displaySidebar = (boolean === undefined) ? !self.displaySidebar : boolean;

            if (self.displaySidebar) {
                $('body').addClass('modal-open');
            } else {
                $('body').removeClass('modal-open');
            }
        };

        $rootScope.$on('toggleSidebar', function(event, data) {
            console.log('WOW');
            self.toggleSidebar(data)
        });
    }]);

    app.factory('GlobalService', [function () {
        var self = this;


    }]);

    app.directive('movie', [function () {
        return {
            replace: true,
            templateUrl: 'controller/movie/view/movie-card.html'
        }
    }]);

    app.directive('menu', [function () {
        return {
            replace: true,
            templateUrl: 'controller/global/view/menu.html'
        }
    }]);

    app.directive('infiniteScroll', ['$window', '$timeout', '$rootScope', function ($window, $timeout, $rootScope) {
        return {
            link: function (scope, element) {
                var minimumTime = 100;
                var now = new Date().getTime();
                var scrollTimer = 0;
                var lastScrollFireTime = 0;

                function processScroll () {
                    var ratio =  document.body.scrollHeight - (document.body.scrollTop + $window.innerHeight);
                    if (ratio < 100) {
                        $rootScope.$broadcast('improveLimit', true);
                    }
                }

                function onScroll () {
                    if (!scrollTimer) {
                        if (now - lastScrollFireTime > minimumTime) {
                            processScroll();
                            lastScrollFireTime = now;
                            now -= 1;
                        }
                        scrollTimer = $timeout(function() {
                            scrollTimer = null;
                            lastScrollFireTime = new Date().getTime();
                            processScroll();
                        }, minimumTime);
                    }
                }

                angular.element($window).bind('scroll', onScroll);

                scope.$on('$destroy', function () {
                    angular.element($window).unbind('scroll', onScroll);
                });
            }
        }
    }]);
})();